# Peazi Code Test

The peazi code test is a sample to do list application.

Applicants should work through some of the tasks listed below.

## Prerequisites

* yarn
* node

## Running the App

Install dependencies by running `yarn`.

Run `yarn start` to run the application on port 8080.

Go to `localhost:8080` in your browser to view the app.

## Validation

### Linting

To run the code linting run the command `yarn lint`.

## Tasks

### Task 1 - BUG

To reproduce:

1. Add an item to the To do list
2. Click Save

Expected: The form should clear once save is clicked
Actual: The form still has the previous values in the form

### Task 2 - BUG

To reproduce:

1. Add two items to the To Do list.
2. Delete one of the items

Expected behaviour: Only the clicked item is deleted
Actual Behaviour: Both items are deleted

### Task 3 - FEATURE

A user should not be able to add items with empty title or description.

Requirements

* The save button should be disabled if the title is blank
* The save button should be disabled if the description is blank
* The save button should not be disabled if the title and description both have values

### Task 4 - BUG

To reproduce

1. Enter a really long title and description for a task
2. Click Save

Expected Behaviour: The title should be limited to one line and cut off with ellipsis on overflow. The description should be limited to two lines and show an ellipsis on overflow.
Actual behaviour: The text will overflow the box for the item.
