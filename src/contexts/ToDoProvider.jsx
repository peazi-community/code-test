import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';

import ToDoContext from './ToDoContext';

const id = uuid();

const ToDoProvider = ({ children }) => {
  const [items, setItems] = useState([]);

  const addItem = useCallback((title, description) => {
    setItems((value) => value.concat([{
      id,
      title,
      description,
    }]));
  }, [setItems]);

  const deleteItem = useCallback((idToDelete) => {
    setItems((value) => value.filter((item) => item.id !== idToDelete));
  }, [setItems]);

  return (
    <ToDoContext.Provider value={{ addItem, items, deleteItem }}>
      {children}
    </ToDoContext.Provider>
  );
};

ToDoProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ToDoProvider;
