import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import './textInput.css';

const TextInput = ({
  type, placeholder, onChange, value,
}) => {
  const handleChange = useCallback((event) => {
    onChange(event.target.value);
  }, [onChange]);

  return (
    <input
      className="TextInput"
      type={type}
      placeholder={placeholder}
      onChange={handleChange}
      value={value}
    />
  );
};

TextInput.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

TextInput.defaultProps = {
  type: 'text',
  placeholder: '',
};

export default TextInput;
