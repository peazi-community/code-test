import React from 'react';
import PropTypes from 'prop-types';

import './clickable.css';

const Clickable = ({ children, onClick }) => {
  if (onClick == null) return <div className="Clickable--noClick">{children}</div>;

  return (
    <button className="Clickable" onClick={onClick} type="button">
      {children}
    </button>
  );
};

Clickable.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.node.isRequired,
};

Clickable.defaultProps = {
  onClick: null,
};

export default Clickable;
