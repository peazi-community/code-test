import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './button.css';

const Button = ({ label, onClick, disabled }) => {
  const handleClick = useCallback(() => {
    if (!disabled) {
      onClick();
    }
  }, [disabled, onClick]);

  return (
    <button
      className={classnames('Button', {
        'Button--disabled': disabled,
      })}
      type="button"
      onClick={handleClick}
    >
      {label}
    </button>
  );
};

Button.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  disabled: false,
};

export default Button;
