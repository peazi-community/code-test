import React from 'react';
import ReactDOM from 'react-dom';

import App from './containers/app/App';
import ToDoProvider from './contexts/ToDoProvider';

import './index.css';

ReactDOM.render((
    <ToDoProvider>
        <App />
    </ToDoProvider>
), document.getElementById('app'));
