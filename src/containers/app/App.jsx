import React, { useCallback, useContext } from 'react';

import ToDoContext from '../../contexts/ToDoContext';
import Item from './components/Item';
import NewItemForm from './components/NewItemForm';

import './app.css';

const App = () => {
  const { items, addItem, deleteItem } = useContext(ToDoContext);

  const handleFormSave = useCallback((values) => {
    addItem(values.title, values.description);
  }, [addItem]);

  return (
    <div className="App">
      <div className="App__Header">
        <div className="App__Title">Peazi</div>
        <div className="App__Subtitle">To Do List</div>
      </div>
      <div className="App__Summary">
        <div className="App__ItemCount">{items.length}</div>
        <div className="App__SummaryLabel">things to do</div>
      </div>
      <NewItemForm onSave={handleFormSave} />
      <div className="App_List">
        {items.map((item) => (
          <Item
            key={item.id}
            item={item}
            onDeleteClick={deleteItem}
          />
        ))}
      </div>
    </div>
  );
};

export default App;
