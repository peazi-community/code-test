import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';

import TextInput from '../../../components/textInput/TextInput';
import Button from '../../../components/button/Button';

import './newItemForm.css';

const NewItemForm = ({ onSave }) => {
  const [formValues, setFormValues] = useState({
    title: '',
    description: '',
  });

  const handleTitleChange = useCallback((title) => {
    setFormValues((value) => ({
      ...value,
      title,
    }));
  }, [setFormValues]);

  const handleDescriptionChange = useCallback((description) => {
    setFormValues((value) => ({
      ...value,
      description,
    }));
  }, [setFormValues]);

  const handleSaveClick = useCallback(() => {
    onSave(formValues);
  }, [formValues, onSave]);

  return (
    <div className="NewItemForm">
      <TextInput
        onChange={handleTitleChange}
        placeholder="Title"
        value={formValues.title}
      />
      <TextInput
        onChange={handleDescriptionChange}
        placeholder="Description"
        value={formValues.description}
      />
      <Button onClick={handleSaveClick} label="Add" />
    </div>
  );
};

NewItemForm.propTypes = {
  onSave: PropTypes.func.isRequired,
};

export default NewItemForm;
