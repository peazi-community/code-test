import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import Delete from '../../../icons/Delete';

import './item.css';
import Clickable from '../../../components/clickable/Clickable';

const Item = ({ item, onDeleteClick }) => {
  const handleDeleteClick = useCallback(() => {
    onDeleteClick(item.id);
  }, [item, onDeleteClick]);

  return (
    <div className="Item">
      <div className="Item__Details">
        <div className="Item__Title">
          {item.title}
        </div>
        <div className="Item__Description">
          {item.description}
        </div>
      </div>
      <div className="Item__Actions">
        <Clickable onClick={handleDeleteClick}>
          <Delete />
        </Clickable>
      </div>
    </div>
  );
};

Item.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  onDeleteClick: PropTypes.func.isRequired,
};

export default Item;
